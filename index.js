module.exports = {
    get BBCode () {
        return require('./BBCode');
    },

    get TagInfo () {
        return require('./TagInfo');
    }
};
